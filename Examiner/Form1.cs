﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Xml.Linq;
using System.Linq;
using System.Windows.Forms;

namespace Examiner
{
    public partial class Form1 : Form
    {
        XDocument Document;
        IEnumerable<XElement> Reader;
        string File;
        int[] Testing; 
        int Current_Question = 0; 
        int Program_Status;  
        int Selected_Answer; 
        int Correct_Answer; 
        int Number_Of_Correct_Answers; 
        int Total_Number_Of_Questions; 

        public Form1()
        {
            InitializeComponent();            
            Main_Window();
        }

        private void Main_Window() // главное окно.
        {
            // название заголовок.
            this.Text = "Экзаменатор";
            // размер окна.
            this.Width = 650;
            this.Height = 425;
            // не отображаются.
            label0.Visible = false; label1.Visible = false; label2.Visible = false;
            label3.Visible = false; label4.Visible = false; label5.Visible = false;
            // отображаются.
            label6.Visible = true; label7.Visible = true; label8.Visible = true;
            label9.Visible = true; label10.Visible = true; label11.Visible = true;
            label12.Visible = true; label13.Visible = true; label14.Visible = true;
            label15.Visible = true; label16.Visible = true; label17.Visible = true;
            // не отображаются.
            radioButton1.Visible = false; radioButton2.Visible = false;
            radioButton3.Visible = false; radioButton4.Visible = false;
            radioButton5.Visible = false; radioButton6.Visible = false;
            // не отображается.
            pictureBox1.Visible = false;
            // не отображается.
            button1.Visible = false;
            // отображаются.
            button2.Visible = true; button3.Visible = true; button4.Visible = true;
            button5.Visible = true; button6.Visible = true; button7.Visible = true;
            // отображаются.
            textBox1.Visible = true; textBox2.Visible = true; textBox3.Visible = true;
            textBox4.Visible = true; textBox5.Visible = true; textBox6.Visible = true;
            textBox7.Visible = true; textBox8.Visible = true;
            // отображается.
            dataGridView1.Visible = true;
            // отображается.
            comboBox1.Visible = true;
        }

        private void Test_Window() // окно тестирования.
        {
            // перемещаются на другой координат.
            label0.Location = new Point(12, 9);
            pictureBox1.Location = new Point(15, 35);
            button1.Location = new Point(15, 357);
            // отображаются.
            label0.Visible = true; label1.Visible = true; label2.Visible = true;
            label3.Visible = true; label4.Visible = true; label5.Visible = true;
            // не отображаются.
            label6.Visible = false; label7.Visible = false; label8.Visible = false;
            label9.Visible = false; label10.Visible = false; label11.Visible = false;
            label12.Visible = false; label13.Visible = false; label14.Visible = false;
            label15.Visible = false; label16.Visible = false; label17.Visible = false;
            // отображаются.
            radioButton1.Visible = true; radioButton2.Visible = true; radioButton3.Visible = true;
            radioButton4.Visible = true; radioButton5.Visible = true; radioButton6.Visible = true;
            // отображается.
            pictureBox1.Visible = true;
            // отображается.
            button1.Visible = true;
            // не отображаются.
            button2.Visible = false; button3.Visible = false; button4.Visible = false;
            button5.Visible = false; button6.Visible = false; button7.Visible = false;
            // не отображаются.
            textBox1.Visible = false; textBox2.Visible = false; textBox3.Visible = false;
            textBox4.Visible = false; textBox5.Visible = false; textBox6.Visible = false;
            textBox7.Visible = false; textBox8.Visible = false;
            // не отображается.
            dataGridView1.Visible = false;
            // не отображается.
            comboBox1.Visible = false;
        }

        private void Button2_Click(object sender, EventArgs e) // регистрация.
        {
            if (textBox1.Text == "" || textBox2.Text == "" || textBox3.Text == "")
            {
                MessageBox.Show("Заполните все поля.", "Ошибка.");
            }
            else if (comboBox1.Text == "")
            {
                MessageBox.Show("Выберите предмет.", "Ошибка.");
            }
            else if (comboBox1.Text == "Математика")
            {
                Maths();
                Number_Of_Correct_Answers = 0;
            }
            else if (comboBox1.Text == "Русский язык")
            {
                MessageBox.Show("Русский язык недоступен.", "Ошибка.");
                //Number_Of_Correct_Answers = 0;
            }
        }

        private void Button3_Click(object sender, EventArgs e) // редактирование.
        {
            try
            {
                // если пустые поля, то сообщает об ошибке.
                if (textBox5.Text == "" || textBox6.Text == "" || textBox7.Text == "" || textBox8.Text == "")
                {
                    MessageBox.Show("Пустые поля.", "Ошибка.");
                }
                else if (dataGridView1.SelectedRows.Count > 0)
                {
                    int n = dataGridView1.SelectedRows[0].Index;
                    dataGridView1.Rows[n].Cells[0].Value = textBox5.Text;
                    dataGridView1.Rows[n].Cells[1].Value = textBox6.Text;
                    dataGridView1.Rows[n].Cells[2].Value = textBox7.Text;
                    dataGridView1.Rows[n].Cells[3].Value = textBox8.Text;
                    Clear_Fields(); // очистит поля.
                }
            }
            catch // если пустая таблица, то сообщает об ошибке.
            {
                MessageBox.Show("Выберите строку для редактирования.", "Ошибка.");
            }
        }        

        private void dataGridView1_MouseClick(object sender, MouseEventArgs e) // выбор нужной строки для редактирования.
        {
            try
            {
                textBox5.Text = dataGridView1.SelectedRows[0].Cells[0].Value.ToString();
                textBox6.Text = dataGridView1.SelectedRows[0].Cells[1].Value.ToString();
                textBox7.Text = dataGridView1.SelectedRows[0].Cells[2].Value.ToString();
                textBox8.Text = dataGridView1.SelectedRows[0].Cells[3].Value.ToString();
            }
            catch // если пустая таблица, то сообщает об ошибке.
            {
                MessageBox.Show("Таблица пустая.", "Ошибка.");
            }
        }

        private void Button4_Click(object sender, EventArgs e) // сохранит данные из таблицы в XML.
        {
            if (dataGridView1.SelectedRows.Count == 0)
            {
                MessageBox.Show("Невозможно сохранить XML файл, т.к таблица пустая. ", "Ошибка.");
            }
            else
            {
                try
                {
                    DataSet Сache = new DataSet(); // создает пустой кэш данных.
                    DataTable Table = new DataTable(); // создает пустую таблицу данных.
                    Table.TableName = "Table"; // название таблицы.
                    Table.Columns.Add("Surname"); // название колонок.
                    Table.Columns.Add("Name");
                    Table.Columns.Add("MiddleName");
                    Table.Columns.Add("Group");
                    Table.Columns.Add("Subject");
                    Table.Columns.Add("Assessment");
                    Сache.Tables.Add(Table); //в "Сache" создается таблица с названием, колонками и созданными выше.
                    foreach (DataGridViewRow F in dataGridView1.Rows)
                    {
                        DataRow String = Сache.Tables["Table"].NewRow(); // создает новую строку в таблице, занесенной в "Сache".
                        String["Surname"] = F.Cells[0].Value; // заносит данные в столбец этой строки из первого столбца "dataGridView1" и дальше ниже.
                        String["Name"] = F.Cells[1].Value;
                        String["MiddleName"] = F.Cells[2].Value;
                        String["Group"] = F.Cells[3].Value;
                        String["Subject"] = F.Cells[4].Value;
                        String["Assessment"] = F.Cells[5].Value;
                        Сache.Tables["Table"].Rows.Add(String); // добавляет эту строку в таблицу "Сache".
                    }
                    Сache.WriteXml("C:\\Users\\Valery\\Desktop\\Rating.xml");
                    MessageBox.Show("XML файл успешно сохранен.", "Выполнено.");
                }
                catch // если пустая таблица, то сообщает об ошибке и не может соранить XML-файл.
                {
                    MessageBox.Show("Невозможно сохранить XML файл.", "Ошибка.");
                }
            }
        }

        private void Button5_Click(object sender, EventArgs e) // очистит таблицу.
        {
            if (dataGridView1.Rows.Count > 0)
            {
                dataGridView1.Rows.Clear(); // очистит таблицу.
                Clear_Fields(); // очистит поля.
            }
            else // если пустая таблица, то сообщает об ошибке.
            {
                MessageBox.Show("Таблица пустая.", "Ошибка.");
            }
        }

        private void Button6_Click(object sender, EventArgs e) // загрузка файла XML в таблицу.
        {
            if (dataGridView1.Rows.Count > 0) // если таблица есть, то сообщает об ошибке и просит очистить таблицу.
            {
                MessageBox.Show("Очистите поле перед загрузкой нового файла.", "Ошибка.");
            }
            else // если нет таблицы.
            {
                if (System.IO.File.Exists("C:\\Users\\Valery\\Desktop\\Rating.xml"))     // если существует данный файл.
                {                                                                        // я хотел написать код "if (File.Exists("C:\\Users\\Valery\\Desktop\\Rating.xml"))", 
                    DataSet Сache = new DataSet(); // создает новый пустой кэш данных.   // но компилятору не нравится и пишет, что метод не найдется. вон там есть директива. компилятор совсем ебанулся??? 
                    Сache.ReadXml("C:\\Users\\Valery\\Desktop\\Rating.xml"); // записывает в таблицу XML-данные из файла.
                    foreach (DataRow String in Сache.Tables["Table"].Rows)
                    {
                        int n = dataGridView1.Rows.Add();
                        dataGridView1.Rows[n].Cells[0].Value = String["Surname"]; // заносит в первый столбец созданной строки данные из первого столбца таблицы "Сache" и дальше ниже. 
                        dataGridView1.Rows[n].Cells[1].Value = String["Name"];
                        dataGridView1.Rows[n].Cells[2].Value = String["MiddleName"];
                        dataGridView1.Rows[n].Cells[3].Value = String["Group"];
                        dataGridView1.Rows[n].Cells[4].Value = String["Subject"];
                        dataGridView1.Rows[n].Cells[5].Value = String["Assessment"];
                    }
                }
                else // если нет файла, то сообщает об ошибке.
                {
                    MessageBox.Show("XML файл не найден.", "Ошибка.");
                }
            }
        }

        private void Button7_Click(object sender, EventArgs e) // удаление строки.
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                dataGridView1.Rows.RemoveAt(dataGridView1.SelectedRows[0].Index);
                Clear_Fields(); // очистит поля.
            }
            else // если пустая таблица, то сообщает об ошибке.
            {
                MessageBox.Show("Выберите строку для удаления.", "Ошибка.");
            }
        }

        private void Maths() // математика.
        {
            Test_Window(); // перейдет в окно тестирования.
            // не отображается.
            pictureBox1.Visible = false;
            // не отображаются.
            radioButton1.Visible = false; radioButton2.Visible = false; radioButton3.Visible = false;
            radioButton4.Visible = false; radioButton5.Visible = false; radioButton6.Visible = false;
            // не отображаются.
            label1.Visible = false; label2.Visible = false; label3.Visible = false;
            label4.Visible = false; label5.Visible = false;
            File = "тест.xml"; // имя файла для тестирования. 
            try
            {
                Document = XDocument.Load(File); // загрузит файл-XML.
                Reader = Document.Elements(); // читает файд-XML.
                this.Text = Reader.Elements("title").ElementAt(0).Value; // выводит название заголовок.
                label0.Text = Reader.Elements("info").ElementAt(0).Value; // выводит описание.
                Total_Number_Of_Questions = Reader.Elements("queries").Elements().Count(); // подсчитает количество вопросов.
                Testing = new int[Total_Number_Of_Questions]; // создает массив и генерирует тестирование.
                Boolean[] Question; // запишет числа тестирования в массив, чтобы каждое число было только один раз в массиве. это вопросы перемешанные.                               
                Question = new Boolean[Total_Number_Of_Questions]; 
                for (int i = 0; i < Total_Number_Of_Questions; i++)
                {
                    Question[i] = false; // если вопрос два раза попадается, то повторный вопрос не отображается.
                }
                Random Random = new Random(); // генератор.
                int Random_Question; // случайный вопрос.
                for (int i = 0; i < Total_Number_Of_Questions; i++)
                {
                    do
                    {
                        Random_Question = Random.Next(Total_Number_Of_Questions);
                    }
                    while (Question[Random_Question] == true);
                    Testing[i] = Random_Question;
                    Question[Random_Question] = true;
                }
                Program_Status = 0; // работа начинается.
                Current_Question = 0; // с нуля количества правильных вопросов.
            }
            catch // если некорректное имя файла или нет файла, то сообщает об ошибке.
            {
                label0.Text = "Файл не найден или удален.";
                MessageBox.Show("Ошибка доступа к файлу.\n", "Экзаменатор");
                Program_Status = 2; // работа завершается.
                Clear_Fields(); // очистит поля.
                button7.Text = "Назад";
                return;
            }
        }

        private void Button1_Click(object sender, EventArgs e) // щелчок на кнопке "OK".
        {
            switch (Program_Status)
            {
                case 0: 
                    // первый вопрос отображается.
                    Read_Question(Testing[Current_Question]);
                    Current_Question++;
                    Program_Status = 1;
                    break;
                case 1:
                    // переходит к следующему вопросу.
                    if (Selected_Answer == Correct_Answer) // правильный ли ответ.
                    {
                        Number_Of_Correct_Answers++;
                    }
                    if (Current_Question < Total_Number_Of_Questions)
                    {
                        // отображается следующий вопрос.
                        Read_Question(Testing[Current_Question]);
                        Current_Question = Current_Question + 1;
                    }
                    else // если нет больше вопросов.
                    {
                        // не отображаются.
                        radioButton1.Visible = false; radioButton2.Visible = false;
                        radioButton3.Visible = false; radioButton4.Visible = false;
                        radioButton5.Visible = false;
                        // не отображаются.
                        label1.Visible = false; label2.Visible = false; label3.Visible = false;
                        label4.Visible = false; label5.Visible = false;
                        // не отображается.
                        pictureBox1.Visible = false; 
                        Result(); // выводит оценку и внесет оценку в таблицу.
                        Program_Status = 2; // работа завершится.
                    }
                    break;
                case 2: // тестирование завершится.
                    Main_Window(); // вернется в главное.
                    Clear_Fields(); // очистит поля после завершения тестирования.
                    break;
            }
        }


        private void Clear_Fields() // очистка поля.
        {
            textBox1.Text = ""; textBox2.Text = ""; textBox3.Text = ""; textBox4.Text = "";
            textBox5.Text = ""; textBox6.Text = ""; textBox7.Text = ""; textBox8.Text = "";
            comboBox1.Text = "";
        }

        private void Read_Question(int i) // выводит вопрос и варианты ответа.
        {
            // не отображаются.
            radioButton1.Visible = false; radioButton2.Visible = false;
            radioButton3.Visible = false; radioButton4.Visible = false;
            radioButton5.Visible = false;
            // не отображаются.
            label1.Visible = false; label2.Visible = false; label3.Visible = false;
            label4.Visible = false; label5.Visible = false;
            // отображается.
            radioButton6.Checked = true;
            // выводит вопрос.
            label0.Text = Reader.Elements("queries").Elements().ElementAt(i).Element("q").Value;
            // файл иллюстрации.
            string Illustration_File = Reader.Elements("queries").Elements().ElementAt(i).Element("q").Attribute("src").Value;
            // правильный ответ.
            Correct_Answer = System.Convert.ToInt32(Reader.Elements("queries").Elements().ElementAt(i).Element("q").Attribute("right").Value);
            if (Illustration_File.Length != 0)
            {
                try // отображает изображение, если есть иллюстрация.
                {
                    pictureBox1.Image = new Bitmap(Illustration_File);
                    pictureBox1.Visible = true;
                    radioButton1.Top = pictureBox1.Bottom + 16;
                    label1.Top = radioButton1.Top - 3;
                }
                catch // не отображает изображение и сообщает об ошибке, если файл иллюстрации случайно удален или не найден.
                {
                    if (pictureBox1.Visible)
                    {
                        pictureBox1.Visible = false; // изображение не отображается.
                    }
                    label0.Text += "\n\n\nОшибка доступа к файлу " + "«" + Illustration_File + "»" + "." + "\n\n\n";
                    radioButton1.Top = label0.Bottom + 16;
                    label1.Top = radioButton1.Top - 3;
                }
            }
            else // если нет иллюстрации, тогда отключает форму.
            {
                pictureBox1.Visible = false; // изображение не отображается.
                radioButton1.Top = label0.Bottom + 16;
                label1.Top = radioButton1.Top - 3;

            }
            int Number_Of_Options = 0;
            // показывает варианты вопроса.
            foreach (XElement a in Reader.Elements("queries").Elements().ElementAt(i).Element("as").Elements())
            {
                switch (Number_Of_Options)
                {
                    case 0: // один вариант.
                        label1.Text = a.Value;
                        label1.Visible = true;
                        radioButton1.Visible = true;
                        break;
                    case 1: // два варианта.
                        label2.Text = a.Value;
                        label2.Visible = true;
                        radioButton2.Top = label1.Bottom + 10;
                        label2.Top = radioButton2.Top - 3;
                        radioButton2.Visible = true;
                        break;
                    case 2: // три варианта.
                        label3.Text = a.Value;
                        label3.Visible = true;
                        radioButton3.Top = label2.Bottom + 10;
                        label3.Top = radioButton3.Top - 3;
                        radioButton3.Visible = true;
                        break;
                    case 3: // четыре варианта.
                        label4.Text = a.Value;
                        label4.Visible = true;
                        radioButton4.Top = label3.Bottom + 10;
                        label4.Top = radioButton4.Top - 3;
                        radioButton4.Visible = true;
                        break;
                    case 4: // пять вариантов. 
                        label5.Text = a.Value;
                        label5.Visible = true;
                        radioButton5.Top = label4.Bottom + 10;
                        label5.Top = radioButton5.Top - 3;
                        radioButton5.Visible = true;
                        break;
                }
                Number_Of_Options++;
            }
            button1.Enabled = false;
        }

        private void radioButton1_Click(object sender, EventArgs e) // щелчок на кнопке выбора ответа.
        {
            if ((RadioButton)sender == radioButton1)
            {
                Selected_Answer = 1;
            }
            if ((RadioButton)sender == radioButton2)
            {
                Selected_Answer = 2;
            }
            if ((RadioButton)sender == radioButton3)
            {
                Selected_Answer = 3;
            }
            if ((RadioButton)sender == radioButton4)
            {
                Selected_Answer = 4;
            }
            if ((RadioButton)sender == radioButton5)
            {
                Selected_Answer = 5;
            }
            button1.Enabled = true;
        }

        private void Result() // результат и внесет данные в таблицу после завершения тестирования.
        {
            int i;
            int n = dataGridView1.Rows.Add();            
            int Number_Of_Points; // количество баллов. 
            int Number_Of_Responses = 0; // количество правильных ответов, необходимых для оценки. 
            Number_Of_Points = Reader.Elements("levels").Elements().Count();
            for (i = 0; i < Number_Of_Points - 1; i++)
            {
                Number_Of_Responses = System.Convert.ToInt32(Reader.Elements("levels").Elements().ElementAt(i).Attribute("p").Value);
                if (Number_Of_Correct_Answers >= Number_Of_Responses) // количество правильных ответов больше или равно необходимому для оценки.
                {                                                   
                    break;
                }
            }
            // выводит результат. 
            label0.Text = "Всего вопросов: " + Total_Number_Of_Questions.ToString() + "\n" + "Правильных ответов: " + Number_Of_Correct_Answers.ToString() + "\n" + "Оценка: " + Reader.Elements("levels").Elements().ElementAt(i).Value;
            // внесет данные и оценку в таблицу.
            dataGridView1.Rows[n].Cells[0].Value = textBox1.Text;
            dataGridView1.Rows[n].Cells[1].Value = textBox2.Text;
            dataGridView1.Rows[n].Cells[2].Value = textBox3.Text;
            dataGridView1.Rows[n].Cells[3].Value = textBox4.Text;
            dataGridView1.Rows[n].Cells[4].Value = comboBox1.Text;
            dataGridView1.Rows[n].Cells[5].Value = Reader.Elements("levels").Elements().ElementAt(i).Value;
        }        
    }
}